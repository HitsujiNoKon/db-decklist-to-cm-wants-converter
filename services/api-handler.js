const axios = require('axios');
const Importer = require('./decklist-importer')

const importer = new Importer()
class ApiHandler{
    constructor(apiHandler) {
        this.apiHandler = apiHandler
    }

    async getCardNames() {
        var decklist = await importer.parseDecklist(',')
        var params = new URLSearchParams([['id', decklist]])
        var cardNames = []
        return new Promise((resolve, reject) => {
            axios.get('https://db.ygoprodeck.com/api/v7/cardinfo.php', { params })
            .then(response => {
                Object.keys(response.data.data).forEach(key => {
                    cardNames.push({
                        id: response.data.data[key].id,
                        name: response.data.data[key].name
                    })
                })
                resolve(cardNames);
            })
            .catch(err => {
                reject(err);
            });
        })
    }
}

module.exports = ApiHandler