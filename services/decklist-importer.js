const Promise = require('bluebird');
const { sep } = require('path');
const fs = Promise.promisifyAll(require('fs'));

class Importer{
    constructor(importer) {
        this.importer = importer
    }
    
    async parseDecklist( separator ) {
        return new Promise((resolve, reject) => {
            fs.readFileAsync('./decklist.ydk', 'utf-8')
            .then(data => {
                var decklist = data.toString().split("\n");
                decklist = decklist.filter((value) => {
                    if (/^\d+$/.test(value)) {
                        return value
                    }
                })
                decklist = decklist.map( x => {
                    return parseInt(x, 10)
                })
                if (separator == null) {
                    resolve(decklist)
                } else {
                    resolve(decklist.join(separator))
                }
                
            })
            .catch(err => {
                reject(err)
            })
        })
    }
}

module.exports = Importer