const ApiHandler = require('./services/api-handler')
const Importer = require('./services/decklist-importer')

const apiHandler = new ApiHandler()
const importer = new Importer()


async function getCMList() {
    var cmList = []
    var deckList = await importer.parseDecklist(null)
    var cardNames = await apiHandler.getCardNames()
    let cardCount = {}

    deckList.forEach( card => {
        cardCount[card] = (cardCount[card] || 0) + 1
    })

    cardNames.forEach( card => {
        cmList.push({
            count: cardCount[card.id],
            name: card.name
        })
    });
    cmList.forEach( key => {
        console.log(key.count + ' ' + key.name)
    })
}

getCMList()